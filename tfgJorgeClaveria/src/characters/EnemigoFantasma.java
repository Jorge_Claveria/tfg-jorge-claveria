package characters;

import com.badlogic.gdx.math.Rectangle;

import managers.ResourceManager;
/**
 * Clase del enemigo fantasma
 * @author usuario
 *
 */
public class EnemigoFantasma extends Enemigo {
	public EnemigoFantasma(float x, float y,float speed) {
		super(x, y,speed);
		animation = ResourceManager.getAnimation("ghost");
		setRect(new Rectangle(x, y, 40, 65));
		setLives(4);
		
		// TODO Auto-generated constructor stub
	}
	/**
	 * Metodo que controla el movimiento y actualiza al fantasma
	 * @param dt
	 * @param tocar
	 */
public void update(float dt,boolean tocar) {
		
		super.update(dt);
		if(!tocar){
		setX(getX() - getSpeed() * dt);
		setRectX(getX());
		}
	}
}
