package characters;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import managers.ResourceManager;
/**
 * Clase para la trampa
 * @author usuario
 *
 */
public class Trampa extends Bala {
	/**
	 * Metodo constructor de la trampa
	 * @param x
	 * @param y
	 * @param speed
	 */
	public Trampa(float x, float y,float speed) {
		super(x, y,speed);
		// TODO Auto-generated constructor stub
		texture = ResourceManager.getTexture("trap");
		setRect(new Rectangle(x, y, texture.getWidth(), texture.getHeight()));
		setLives(1);
	}

	@Override
	/**
	 * Metodo que dibuja la trampa
	 */
	public void draw(SpriteBatch batch) {
		// TODO Auto-generated method stub
		batch.draw(texture, getX(), getY());
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		
	}
}
