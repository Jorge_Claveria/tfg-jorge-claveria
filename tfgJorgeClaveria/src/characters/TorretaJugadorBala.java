package characters;

import com.badlogic.gdx.math.Rectangle;

import managers.ResourceManager;

/**
 * Proyectiles que lanza el jugador
 * @author Jorge Claveria
 *
 */
public class TorretaJugadorBala extends Bala {

	/**
	 * Metodo constructor de los disparos del jugador
	 * @param x Posición inicial x
	 * @param y Posición inicial y
	 * @param speed Velocidad en x
	 */
	public TorretaJugadorBala(float x, float y, float speed) {
		super(x, y, speed);
		
		texture = ResourceManager.getTexture("turret_bullet");
		setRect(new Rectangle(x, y, texture.getWidth(), texture.getHeight()));
	}

	@Override
	/**
	 * Metodo que actualiza las balas y su movimiento
	 */
	public void update(float dt) {
		setX(getX() + getSpeed() * dt);
		setRectX(getX());
	}
}
