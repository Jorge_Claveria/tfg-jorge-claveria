package characters;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import static util.Constants.SCREEN_HEIGHT;
import static util.Constants.SCREEN_WIDTH;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import managers.ConfigurationManager;
import managers.ResourceManager;
import managers.SpriteManager;
import util.Constants;
/**
 * Clase que controla las casillas y que defensas tienen ademas de controlar 
 * el selector de casilla y la puntuación
 * @author usuario
 *
 */
public class Casilla extends Character{
	public int puntuacion;
	private ConfigurationManager configurationManager;
	private float moveTime;
	private List<Valla> vallas;
	private List<Trampa> trampas;
	private List<TorretaDefensa> torretaDefensa;
	private List<Tesla> tesla;
	public int getPuntuacion() {
		return puntuacion;
	}

	public void setPuntuacion(int puntuacion) {
		this.puntuacion = puntuacion;
	}
	public List<Tesla> getTesla() {
		return tesla;
	}
	public void setTesla(List<Tesla> tesla) {
		this.tesla = tesla;
	}
	public List<TorretaDefensa> getTorretaDefensa() {
		return torretaDefensa;
	}
	public void setTorretaDefensa(List<TorretaDefensa> torretaDefensa) {
		this.torretaDefensa = torretaDefensa;
	}
	public List<Trampa> getTrampas() {
		return trampas;
	}
	public void setTrampas(List<Trampa> trampas) {
		this.trampas = trampas;
	}
	public List<Valla> getVallas() {
		return vallas;
	}
	public void setVallas(List<Valla> vallas) {
		this.vallas = vallas;
	}

	/**
	 * Metodo constructor de casilla
	 * @param x
	 * @param y
	 * @param speed
	 * @param configurationManager
	 */
	public Casilla(float x, float y,float speed,ConfigurationManager configurationManager) {
		super(x, y);
		// TODO Auto-generated constructor stub
		puntuacion = 0;
		vallas = new ArrayList<Valla>();
		trampas = new ArrayList<Trampa>();
		torretaDefensa = new ArrayList<TorretaDefensa>();
		tesla = new ArrayList<Tesla>();
		animation = ResourceManager.getAnimation("casilla");
		setRect(new Rectangle(x, y, Constants.TURRET_WIDTH, Constants.TURRET_HEIGHT));
		
		this.configurationManager = configurationManager;
	}
	/**
	 * Metodo que dibuja el selector de casilla
	 */
	@Override
	public void draw(SpriteBatch batch) {
		// TODO Auto-generated constructor stub
		batch.draw(currentFrame, getX(), getY());
	}
	/**
	 * Metodo que actualiza las casillas y sus elementos
	 * @param dt
	 * @param spriteManager
	 */
	public void update(float dt, SpriteManager spriteManager) {
		
		stateTime += Gdx.graphics.getDeltaTime();
		currentFrame = animation.getKeyFrame(stateTime, true);
		
		moveTime += dt;
		
		
		if (Gdx.input.isKeyPressed(Keys.S)) {
			if (moveTime >= getBulletRate()) {
				moveTime = 0;
			if (getY() > 160){
				setY(getY() - 80);
		}
		}
		}
		
		if (Gdx.input.isKeyPressed(Keys.W)) {
			if (moveTime >= getBulletRate()) {
				moveTime = 0;
			if (getY() < 450){
				setY(getY() + 80);
		}
		}
		}
		if (Gdx.input.isKeyPressed(Keys.A)) {
			if (moveTime >= getBulletRate()) {
				moveTime = 0;
			if (getX() > 220){
				setX(getX() - 80);
		}
		}
		}
		if (Gdx.input.isKeyPressed(Keys.D)) {
			if (moveTime >= getBulletRate()) {
				moveTime = 0;
			if (getX() < 540){
				setX(getX() + 80);
		}
		}
		}
		if (Gdx.input.isKeyPressed(Keys.Q)) {
			if (moveTime >= getBulletRate()) {
				moveTime = 0;
				if(puntuacion > 300){
		Valla valla = new Valla(getX(),getY(),0);
		getVallas().add(valla);
		puntuacion = puntuacion - 300;
				}
		}
		}
		if (Gdx.input.isKeyPressed(Keys.R)) {
			if (moveTime >= getBulletRate()) {
				moveTime = 0;
				if(puntuacion > 300){
		Trampa trampa = new Trampa(getX(),getY(),0);
		getTrampas().add(trampa);
		puntuacion = puntuacion - 300;
				}
		}
		}
		if (Gdx.input.isKeyPressed(Keys.E)) {
			if (moveTime >= getBulletRate()) {
				moveTime = 0;
				if(puntuacion > 300){
		TorretaDefensa torretaDefensa = new TorretaDefensa(getX(),getY(),0);
		getTorretaDefensa().add(torretaDefensa);
		puntuacion = puntuacion - 300;
				}
		}
		}
		if (Gdx.input.isKeyPressed(Keys.T)) {
			if (moveTime >= getBulletRate()) {
				moveTime = 0;
				if(puntuacion > 300){
		Tesla torretaDefensa = new Tesla(getX(),getY(),0);
		getTesla().add(torretaDefensa);
		puntuacion = puntuacion - 300;
				}
		}
		}
		
		setRectX(getX());
		setRectY(getY());
		
		// Disparo estándar
		
	}

}
