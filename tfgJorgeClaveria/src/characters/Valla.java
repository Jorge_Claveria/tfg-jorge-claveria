package characters;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import managers.ResourceManager;
import util.Constants;
/**
 * Clase de la valla
 * @author usuario
 *
 */
public class Valla extends Bala {
	/**
	 * Metodo constructor de la valla
	 * @param x
	 * @param y
	 * @param speed
	 */
	public Valla(float x, float y,float speed) {
		super(x, y,speed);
		// TODO Auto-generated constructor stub
		texture = ResourceManager.getTexture("fence");
		setRect(new Rectangle(x, y, texture.getWidth(), texture.getHeight()));
		setLives(10);
	}

	@Override
	/**
	 * Metodo que dibuja la valla
	 */
	public void draw(SpriteBatch batch) {
		// TODO Auto-generated method stub
		batch.draw(texture, getX(), getY());
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		
	}

	

}
