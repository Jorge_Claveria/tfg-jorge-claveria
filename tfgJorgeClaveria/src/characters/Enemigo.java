package characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
/**
 * Clase base para los enemigos del juego
 * @author usuario
 *
 */
public class Enemigo extends Character {
	@Override
	public String toString() {
		return "Enemigo [speed=" + speed + ", x=" + x + ", y=" + y + "]";
	}

	public enum EnemyType {
		NORMAL, FANTASMA, POTENCIADO, RESISTENTE
	}
	/**
	 * Constructor de enemigo
	 * @param x
	 * @param y
	 */
	public Enemigo(float x, float y) {
		super(x, y);
	}
	/**
	 * Constructor de enemigos con velocidad
	 * @param x
	 * @param y
	 * @param speed
	 */
	public Enemigo(float x, float y, float speed) {
		super(x, y, speed);
	}
	/**
	 * Actualiza los enemigos
	 * @param dt
	 */

public void update(float dt) {
		
		stateTime += Gdx.graphics.getDeltaTime();
		currentFrame = animation.getKeyFrame(stateTime, true);
	}
	/**
	 * Dibuja los enemigos
	 */
	public void draw(SpriteBatch batch) {
		batch.draw(currentFrame, getX(), getY());
	}
}
