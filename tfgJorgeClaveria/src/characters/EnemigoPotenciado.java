package characters;

import com.badlogic.gdx.math.Rectangle;

import managers.ResourceManager;
/**
 * Clase de los enemigos potenciados
 * @author usuario
 *
 */
public class EnemigoPotenciado extends Enemigo{
	/**
	 * Metodo constructor de un enemigo potenciado
	 * @param x
	 * @param y
	 * @param speed
	 */
	public EnemigoPotenciado(float x, float y,float speed) {
		super(x, y,speed);
		animation = ResourceManager.getAnimation("potenciado");
		setRect(new Rectangle(x, y, 40, 65));
		setLives(6);
		
		// TODO Auto-generated constructor stub
	}
	/**
	 * Metodo para actualizar el enemigo potenciado y controlar su movimiento
	 * @param dt
	 * @param tocar
	 */
public void update(float dt,boolean tocar) {
		
		super.update(dt);
		if(!tocar){
		setX(getX() - getSpeed() * dt);
		setRectX(getX());
		}
	}
}
