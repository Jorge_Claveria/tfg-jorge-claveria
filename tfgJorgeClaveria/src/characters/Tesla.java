package characters;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import managers.ResourceManager;
/**
 * Clase para el cañon tesla
 * @author usuario
 *
 */
public class Tesla extends Bala{
	private List<Bala> bullets;
	private float bulletTime;
	/**
	 * Metodo constructor del cañon tesla
	 * @param x
	 * @param y
	 * @param speed
	 */
	public Tesla(float x, float y,float speed) {
		super(x, y,speed);
		bullets = new ArrayList<Bala>();
		// TODO Auto-generated constructor stub
		texture = ResourceManager.getTexture("tesla");
		setRect(new Rectangle(x, y, texture.getWidth(), texture.getHeight()));
		setLives(10);
	}

	@Override
	/**
	 * Metodo para dibujar el cañon tesla
	 */
	public void draw(SpriteBatch batch) {
		// TODO Auto-generated method stub
		batch.draw(texture, getX(), getY());
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		
	}
	public List<Bala> getBullets() {
		return bullets;
	}
	
	public void setBullets(List<Bala> bullets) {
		this.bullets = bullets;
	}

}
