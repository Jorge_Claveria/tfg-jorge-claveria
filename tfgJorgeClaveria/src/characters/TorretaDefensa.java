package characters;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import managers.ResourceManager;
/**
 * Clase de la torreta de defensa
 * @author usuario
 *
 */
public class TorretaDefensa extends Bala {
	private List<Bala> bullets;
	private float bulletTime;
	/**
	 * Metodo constructor de la torreta de defensa
	 * @param x
	 * @param y
	 * @param speed
	 */
	public TorretaDefensa(float x, float y,float speed) {
		super(x, y,speed);
		bullets = new ArrayList<Bala>();
		// TODO Auto-generated constructor stub
		texture = ResourceManager.getTexture("defensa");
		setRect(new Rectangle(x, y, texture.getWidth(), texture.getHeight()));
		setLives(10);
	}

	@Override
	/**
	 * Metodo para dibujar la torreta de defensa
	 */
	public void draw(SpriteBatch batch) {
		// TODO Auto-generated method stub
		batch.draw(texture, getX(), getY());
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		
		
	}
	public List<Bala> getBullets() {
		return bullets;
	}
	
	public void setBullets(List<Bala> bullets) {
		this.bullets = bullets;
	}
	/**
	 * Metodo de disparo de la torreta de defensa
	 * @param dt
	 */
public void shoot(float dt) {
	bulletTime += dt;
	if (bulletTime >= getBulletRate()) {
		bulletTime = 0;
		Bala bullet = new TorretaJugadorBala(getX() + 50, getY()+20, getBulletSpeed());
		getBullets().add(bullet);
	}
	}
}
