package characters;

import com.badlogic.gdx.math.Rectangle;

import managers.ResourceManager;
/**
 * Clase del enemigo resistente
 * @author usuario
 *
 */
public class EnemigoResistente extends Enemigo {
	/**
	 * Metodo constructor de un enemigo resistente
	 * @param x
	 * @param y
	 * @param speed
	 */
	public EnemigoResistente(float x, float y,float speed) {
		super(x, y,speed);
		animation = ResourceManager.getAnimation("resistente");
		setRect(new Rectangle(x, y, 40, 65));
		setLives(10);
		
		// TODO Auto-generated constructor stub
	}
	/**
	 * Metodo para actualizar el enemigo potenciado y controlar su movimiento
	 * @param dt
	 * @param tocar
	 */
public void update(float dt,boolean tocar) {
		
		super.update(dt);
		if(!tocar){
		setX(getX() - getSpeed() * dt);
		setRectX(getX());
		}
	}
}
