package characters;

import com.badlogic.gdx.math.Rectangle;

import managers.ResourceManager;
import util.Constants;
/**
 * Clase del enemigo normal
 * @author usuario
 *
 */
public class EnemigoNormal extends Enemigo {
/**
 * Metodo constructor de un enemigo normal
 * @param x
 * @param y
 * @param speed
 */
	public EnemigoNormal(float x, float y,float speed) {
		super(x, y,speed);
		animation = ResourceManager.getAnimation("zombie");
		setRect(new Rectangle(x, y, 40, 65));
		setLives(4);
		
		
		// TODO Auto-generated constructor stub
	}
	/**
	 * Metodo que actualiza y controla el movimiento del enemigo normal 
	 * @param dt
	 * @param tocar
	 */
public void update(float dt,boolean tocar) {
		
		super.update(dt);
		if(!tocar){
		setX(getX() - getSpeed() * dt);
		setRectX(getX());
		}
	}

}
