package characters;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Es la clase base para los proyectiles
 * @author Jorge Claveria
 *
 */
public abstract class Bala extends Character {

	protected Texture texture;
	
	/**
	 * 
	 * @param x Posición inicial x
	 * @param y Posición inicial y
	 * @param speed Velocidad en x
	 */
	public Bala(float x, float y, float speed) {
		super(x, y, speed);
	}
	
	public void draw(SpriteBatch batch) {
		
		batch.draw(texture, getX(), getY());
	}
	
	public abstract void update(float dt);

}
