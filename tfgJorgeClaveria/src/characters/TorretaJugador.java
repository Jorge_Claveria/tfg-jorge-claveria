package characters;

import static util.Constants.SCREEN_HEIGHT;
import static util.Constants.SCREEN_WIDTH;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import managers.ConfigurationManager;
import managers.ResourceManager;
import managers.SpriteManager;
import util.Constants;

/**
 * Personaje del jugador
 * @author Jorge Claveria
 *
 */
public class TorretaJugador extends Character {

	// Proyectiles disparados 
	private List<Bala> bullets;
	private float bulletTime;

	
	private ConfigurationManager configurationManager;
	
	/**
	 * Metodo constructor del personaje del jugador
	 * @param x Posición inicial x
	 * @param y Posición inicial y
	 * @param speed Velocidad en x e y
	 * @param configurationManager 
	 */
	public TorretaJugador(float x, float y, float speed, ConfigurationManager configurationManager) {
		super(x, y, speed);
		
		bullets = new ArrayList<Bala>();
	
		animation = ResourceManager.getAnimation("turret");
		setRect(new Rectangle(x, y, Constants.TURRET_WIDTH, Constants.TURRET_HEIGHT));
		
		this.configurationManager = configurationManager;
	}
	
	public List<Bala> getBullets() {
		return bullets;
	}
	
	public void setBullets(List<Bala> bullets) {
		this.bullets = bullets;
	}
	
	
	/**
	 * Dibuja el personaje en pantalla
	 */
	@Override
	public void draw(SpriteBatch batch) {
		
		batch.draw(currentFrame, getX(), getY());
	}
	
	/**
	 * Actualiza el movimiento
	 * @param dt
	 * @param spriteManager
	 */
	public void update(float dt, SpriteManager spriteManager) {
		
		stateTime += Gdx.graphics.getDeltaTime();
		currentFrame = animation.getKeyFrame(stateTime, true);
		
		bulletTime += dt;
		
		
		
		if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			if (bulletTime >= getBulletRate()) {
				bulletTime = 0;
			if (getY() > 160){
				setY(getY() - 80);
		}
		}
		}
		
		if (Gdx.input.isKeyPressed(Keys.UP)) {
			if (bulletTime >= getBulletRate()) {
				bulletTime = 0;
			if (getY() < 450){
				setY(getY() + 80);
		}
		}
		}
		
		setRectX(getX());
		setRectY(getY());
		
		// Disparo estándar
		if (Gdx.input.isKeyPressed(Keys.SPACE)) {

			if (bulletTime >= getBulletRate()) {
				bulletTime = 0;
				
				// Dispara un proyectil
				shoot();
			}
		}
	}
		
	
	/**
	 * Dispara un proyectil
	 */
	public void shoot() {
		
		Bala bullet = new TorretaJugadorBala(getX() + 50, getY()+20, getBulletSpeed());
		getBullets().add(bullet);
		ResourceManager.getSound("shoot").play();
		
	
	}

	
	
}
