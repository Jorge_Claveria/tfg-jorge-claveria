package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;


import tfgJorgeClaveria.TfgJorgeClaveria;
import util.Constants;

/**
 * Menú principal del juego
 * @author Jorge Claveria
 *
 */
public class MainMenuScreen implements Screen {
	
	TfgJorgeClaveria game;
	Stage stage;
	
	public MainMenuScreen(TfgJorgeClaveria game) {
		this.game = game;
		
	}
	/**
	 * Carga la pantalla
	 */
	private void loadScreen() {
				
		// Grafo de escena que contendrá todo el menú
		stage = new Stage();
					
		// Crea una tabla, donde añadiremos los elementos de menú
		Table table = new Table();
		table.setPosition(Constants.SCREEN_WIDTH / 2.5f, Constants.SCREEN_HEIGHT / 1.5f);
		// La tabla ocupa toda la pantalla
	    table.setFillParent(true);
	    table.setHeight(500);
	    stage.addActor(table);
		
	    // Etiqueta de texto
		Label label = new Label("TANKS VS MONSTERS", game.getSkin());
		table.addActor(label);
		
		// Botón
		TextButton buttonPlay = new TextButton("JUGAR", game.getSkin());
		buttonPlay.setPosition(label.getOriginX()-20, label.getOriginY()-120);
		buttonPlay.setWidth(200);
		buttonPlay.setHeight(40);
		buttonPlay.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				
				dispose();
				game.setScreen(new GameScreen(game));
			}
		});
		table.addActor(buttonPlay);
		
		// Botón
		TextButton buttonLevel = new TextButton("SELECCIONAR NIVEL", game.getSkin());
		buttonLevel.setPosition(label.getOriginX()-20, label.getOriginY()-200);
		buttonLevel.setWidth(200);
		buttonLevel.setHeight(40);
		buttonLevel.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				
				dispose();
				game.setScreen(new LevelScreen(game));
				
			}
		});
		table.addActor(buttonLevel);
		
		// Botón
		TextButton buttonQuit = new TextButton("SALIR", game.getSkin());
		buttonQuit.setPosition(label.getOriginX()-20, label.getOriginY()-280);
		buttonQuit.setWidth(200);
		buttonQuit.setHeight(40);
		buttonQuit.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;	
			}
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				
				game.dispose();
				System.exit(0);
			}
		});
		table.addActor(buttonQuit);
		
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	/**
	 * Muestra la pantalla
	 */
	public void show() {

		loadScreen();
	}
	
	@Override
	public void render(float dt) {

		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        
		// Pinta el menú
		stage.act(dt);
		stage.draw();
	}
	
	@Override
	public void dispose() {

		stage.dispose();
	}

	@Override
	public void hide() {

		
	}

	@Override
	public void pause() {

		
	}

	

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}
}
