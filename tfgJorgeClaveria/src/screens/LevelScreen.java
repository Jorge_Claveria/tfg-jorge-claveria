package screens;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import tfgJorgeClaveria.TfgJorgeClaveria;
import util.Constants;
/**
 * Clase de la pantalla de seleccion de nivel
 * @author usuario
 *
 */
public class LevelScreen implements Screen {
	TfgJorgeClaveria game;
	Stage stage;
	/**
	 * Metodo constructor de la pantalla
	 * @param game
	 */
	public LevelScreen(TfgJorgeClaveria game) {
		this.game = game;
		
	}
	/**
	 * Metodo que carga la pantalla
	 */
	private void loadScreen() {
		
		// Grafo de escena que contendrá todo el menú
		stage = new Stage();
					
		// Crea una tabla, donde añadiremos los elementos de menú
		Table table = new Table();
		table.setPosition(Constants.SCREEN_WIDTH / 2.5f, Constants.SCREEN_HEIGHT / 1.5f);
		// La tabla ocupa toda la pantalla
	    table.setFillParent(true);
	    table.setHeight(500);
	    stage.addActor(table);
		
	    // Etiqueta de texto
		Label label = new Label("          ELIGE NIVEL", game.getSkin());
		table.addActor(label);
		TextButton button1 = new TextButton("Nivel 1", game.getSkin());
		button1.setPosition(label.getOriginX()-20, label.getOriginY()-80);
		button1.setWidth(200);
		button1.setHeight(40);
		button1.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				
				dispose();
				game.level=1;
			}
		});
		table.addActor(button1);
		TextButton button2 = new TextButton("Nivel 2", game.getSkin());
		button2.setPosition(label.getOriginX()-20, label.getOriginY()-140);
		button2.setWidth(200);
		button2.setHeight(40);
		button2.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				
				dispose();
				game.level=2;
			}
		});
		table.addActor(button2);
		TextButton button3 = new TextButton("Nivel 3", game.getSkin());
		button3.setPosition(label.getOriginX()-20, label.getOriginY()-200);
		button3.setWidth(200);
		button3.setHeight(40);
		button3.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				
				dispose();
				game.level=3;
			}
		});
		table.addActor(button3);
		//Boton
		TextButton buttonMainMenu = new TextButton("Volver", game.getSkin());
		buttonMainMenu.setPosition(label.getOriginX()-20, label.getOriginY() - 260);
		buttonMainMenu.setWidth(200);
		buttonMainMenu.setHeight(40);
		buttonMainMenu.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;	
			}
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				
				dispose();
				game.setScreen(new MainMenuScreen(game));
			}
		});
		table.addActor(buttonMainMenu);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float dt) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        
		// Pinta el menú
		stage.act(dt);
		stage.draw();
	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		loadScreen();
	}

}
