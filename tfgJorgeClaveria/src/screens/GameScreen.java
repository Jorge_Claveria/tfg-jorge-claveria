package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import characters.TorretaJugador;
import managers.LevelManager;
import managers.ResourceManager;
import managers.SpriteManager;
import tfgJorgeClaveria.TfgJorgeClaveria;
import util.Constants;

/**
 * Pantalla de juego
 * @author Jorge Claveria
 *
 */
public class GameScreen implements Screen {
	TfgJorgeClaveria game;
	private SpriteManager spriteManager;
	/**
	 * Metodo constructor del GameScreen
	 * @param game
	 */
	public GameScreen(TfgJorgeClaveria game) {
		this.game = game;
		loadScreen();
	}
	
	/**
	 * Carga la pantalla 
	 */
	public void loadScreen() {
		// Carga de todos los recursos del juego (gráficos, sonidos, . . .)
		ResourceManager.loadAllResources();		
		// Inicializa los elementos del juego
		spriteManager = new SpriteManager(game);
		
	}

	/**
	 * Coloca al juego en pausa cada vez que esta pantalla se oculta
	 * 
	 */
	@Override
	public void show() {
		game.paused = false;
	}

	@Override
	public void render(float dt) {
		
		Gdx.gl.glClearColor(0, 1, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        game.camera.update();
		update(dt);
		game.batch.begin();
			// Pinta en pantalla todos los elementos del juego
			spriteManager.draw(game.batch);	
			game.font.draw(game.batch, "PUNTOS: " + spriteManager.casilla.getPuntuacion(), 15, 20);
			
			game.batch.draw(ResourceManager.getTexture("live"), 15, 30);
			game.font.draw(game.batch, "X " + spriteManager.getTurret().getLives(), 50, 40);
			
			
		game.batch.end();
	}
	private void update(float dt) {
		
		if (!game.paused) {
			spriteManager.update(dt);
			spriteManager.getTurret().update(dt, spriteManager);
			spriteManager.getCasilla().update(dt, spriteManager);
		}
		
	}
	@Override
	public void dispose() {
		
	}

	/**
	 * Cuando esta pantalla se oculta, se pausa
	 */
	@Override
	public void hide() {
		
		game.paused = true;
	}

	@Override
	public void pause() {
		
		game.paused = true;
	}

	@Override
	public void resize(int arg0, int arg1) {
	}

	@Override
	public void resume() {
		game.paused = false;
	}
}
