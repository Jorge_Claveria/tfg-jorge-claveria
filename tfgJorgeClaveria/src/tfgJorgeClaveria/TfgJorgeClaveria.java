package tfgJorgeClaveria;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import managers.ConfigurationManager;
import screens.MainMenuScreen;
import util.Constants;

/**
 * Clase principal del juego
 * @author Jorge Claveria
 *
 */
public class TfgJorgeClaveria extends Game {
	

	private Skin skin;
	
	public OrthographicCamera camera;
	public SpriteBatch batch;
	public BitmapFont font;
	public int level;
	public int score;
	public boolean paused;
	public ConfigurationManager configurationManager;

/**
 * Metodo constructor de TfgJorgeClaveria
 */
	public TfgJorgeClaveria() {
		super();
	}

	@Override
	public void create() {
		
		
		
		camera = new OrthographicCamera();
		camera.setToOrtho(false, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
		camera.update();
		
		batch = new SpriteBatch();
		font = new BitmapFont();
		level = 1;
		setScreen(new MainMenuScreen(this));
	}
	
	@Override
	public void render() {
		super.render();
	}
	
	public Skin getSkin() {
        if(skin == null ) {
            skin = new Skin(Gdx.files.internal("ui/uiskin.json"));
        }
        return skin;
    }	
}
