package util;

/**
 * Constantes del juego
 * @author Jorge Claveria
 */
public interface Constants {

	public static final String APP = "Plant vs Zombies";
	
	// Ancho y alto de la pantalla de juego
	public static final int SCREEN_WIDTH = 800;
	public static final int SCREEN_HEIGHT = 600;
	
	
	// Tamaño de la torreta del personaje principal
	public static final int TURRET_WIDTH = 30;
	public static final int TURRET_HEIGHT = 29;
	
	// Tamaño de los personajes estándar 
	public static final int ENEMY_WIDTH = 40;
	public static final int ENEMY_HEIGHT = 30;
	
	//Vida de la torrera
	public static final int TURRET_LIVES = 3;
	
}
