package managers;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;

import characters.Enemigo;
import characters.Enemigo.EnemyType;
import characters.EnemigoFantasma;
import characters.EnemigoNormal;
import characters.EnemigoPotenciado;
import characters.EnemigoResistente;
import screens.GameScreen;
import screens.MainMenuScreen;
import tfgJorgeClaveria.TfgJorgeClaveria;

/**
 * Clase que gestiona y genera los niveles del juego
 * @author usuario
 *
 */
public class LevelManager {
	private static float STEP = 9f;
	// Nivel actual
		public static int currentLevel = 1;
		private SpriteManager spriteManager;
		// Marca el tiempo entre la generación de dos enemigos seguidos
		// Sólo se utiliza para generar niveles aleatorios
		private float enemyTime;
		// Marca el tiempo entre la generación de dos powerups seguidos
		private float powerupTime;
		// Tiempo de paso en la creación de escuadrones
		private float stepTime;
		// Indica el siguiente momento en que se generarán enemigos
		private int currentStep;
		// Indica si se ha terminado el paso actual
		private boolean stepDone;
		private String[] steps;
		public String[] getSteps() {
			return steps;
		}
		public void setSteps(String[] steps) {
			this.steps = steps;
		}

		private Random generator;
		GameScreen gameScreen;
		TfgJorgeClaveria game;
		private List<Enemigo> enemies;
		public boolean finishLevel;
		
		public boolean isFinishLevel() {
			return finishLevel;
		}
		public void setFinishLevel(boolean finishLevel) {
			this.finishLevel = finishLevel;
		}
		public List<Enemigo> getEnemies() {
			return enemies;
		}
		public void setEnemies(List<Enemigo> enemies) {
			this.enemies = enemies;
		}
		/**
		 * Metodo constructor del levelManager
		 * @param level
		 */
		public LevelManager(int level) {
			currentLevel = level;
			enemies = new ArrayList<Enemigo>();
			enemyTime = 0;
			powerupTime = 0;
			stepTime = 0;
			currentStep = 0;
			generator = new Random();
			finishLevel = false;
			
		}
		public static int getCurrentLevel() {
			return currentLevel;
		}
		/**
		 * Metodo para generar el nivel de archivo
		 * @param dt
		 */
		public void generateLevelFromFile(float dt) {
			enemyTime += dt;
			powerupTime += dt;
			stepTime += dt;
			
			if (stepTime >= STEP) {
				currentStep++;
				stepTime = 0;
				stepDone = false;
			}
	if (!stepDone && currentStep < steps.length) {
				// Carga la información del paso actual
				String[] squadrons = steps[currentStep].split(",");
				System.out.println(squadrons[0]);
					if(squadrons[0].equals("NORMAL")){
						enemies.add(new EnemigoNormal(Float.parseFloat(squadrons[1]),Float.parseFloat(squadrons[2]),Float.parseFloat(squadrons[3])));
					}
					if(squadrons[0].equals("FANTASMA")){
						enemies.add(new EnemigoFantasma(Float.parseFloat(squadrons[1]),Float.parseFloat(squadrons[2]),Float.parseFloat(squadrons[3])));
					}
					if(squadrons[0].equals("RESISTENTE")){
						enemies.add(new EnemigoResistente(Float.parseFloat(squadrons[1]),Float.parseFloat(squadrons[2]),Float.parseFloat(squadrons[3])));
					}
					if(squadrons[0].equals("POTENCIADO")){
						enemies.add(new EnemigoPotenciado(Float.parseFloat(squadrons[1]),Float.parseFloat(squadrons[2]),Float.parseFloat(squadrons[3])));
					}
					if(squadrons[0].equals("END")){
						finishLevel = true;
					}
					System.out.println("añadido "+enemies.size());
					stepDone = true;
			}	
	}
		
		/**
		 * Metodo que lee el nivel de archivo
		 */
		public void readCurrentLevelFile() {
			
			FileHandle file = Gdx.files.internal("levels/level" + currentLevel + ".txt");
			String levelInfo = file.readString();
			
			steps = levelInfo.split("\n");
		
		}
		
	
}
