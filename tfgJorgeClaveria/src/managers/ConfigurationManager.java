package managers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

import managers.ConfigurationManager.Score;
import util.Constants;

/**
 * Configuracion del juego
 * @author Jorge Claveria
 *
 */
public class ConfigurationManager {

	private Preferences prefs;
	/**
	 * Metodo constructor
	 */
	public ConfigurationManager() {
		prefs = Gdx.app.getPreferences(Constants.APP);
	}
	/**
	 * Metodo para añadir resultados a la base de datos
	 * @param name
	 * @param score
	 */
public static void addScores(String name, int score) {
		
		try {
			Class.forName("org.sqlite.JDBC");
			
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:sqlite:" + Gdx.files.internal("scores.db"));
		
			Statement statement = connection.createStatement();
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS scores (id integer primary key autoincrement, name text, score int)");
			statement.executeUpdate("INSERT INTO scores (name, score) VALUES ('" + name + "', " + score + ")");
			
			if (statement != null)
				statement.close();
			if (connection != null)
				connection.close();
		
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}

	/**
	 * Devuelve la lista de las diez mejores puntuaciones del juego
	 * @return La lista de puntuaciones
	 */
	public static List<Score> getTopScores() {
		
		try {
			Class.forName("org.sqlite.JDBC");
			
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:sqlite:" + Gdx.files.internal("scores.db"));
		
			Statement statement = connection.createStatement();
			ResultSet res = statement.executeQuery("SELECT name, score FROM scores ORDER BY score DESC LIMIT 10");
			List<Score> scores = new ArrayList<Score>();
			Score score = null;
			while (res.next()) {
				score = new Score();
				score.name = res.getString("name");
				score.score = res.getInt("score");
				scores.add(score);
			}
			
			if (statement != null)
				statement.close();
			if (res != null)
				res.close();
			if (connection != null)
				connection.close();
			
			return scores;
		
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	return new ArrayList<Score>();
	}
	/**
	 * Clase Score
	 * @author usuario
	 *
	 */
	public static class Score {
		@Override
		public String toString() {
			return "Score [name=" + name + ", score=" + score + "]";
		}
		public String name;
		public int score;
	}

	
	
}
