package managers;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Gestiona todos los recursos del juego 
 * @author Jorge Claveria
 *
 */
public class ResourceManager {

	private static Map<String, Texture> textures = new HashMap<String, Texture>();
	private static Map<String, Animation> animations = new HashMap<String, Animation>();
	private static Map<String, Sound> sounds = new HashMap<String, Sound>();
	/**
	 * Metodo que carga todos los recursos
	 */
	public static void loadAllResources() {
		
		Texture.setEnforcePotImages(false);
		// Imágenes
		ResourceManager.loadResource("background", new Texture("backgrounds/background.png"));
		ResourceManager.loadResource("turret_bullet", new Texture("turret/circle_bullet1.png"));
		ResourceManager.loadResource("fence", new Texture("turret/fence.png"));
		ResourceManager.loadResource("defensa", new Texture("turret/defensa.png"));
		ResourceManager.loadResource("tesla", new Texture("turret/tesla.png"));
		ResourceManager.loadResource("trap", new Texture("turret/trap.png"));
		ResourceManager.loadResource("live", new Texture("turret/turretIcon.png"));

		// Animaciones
		ResourceManager.loadResource("turret", new Animation(0.15f, new TextureRegion[]{
				new Sprite(new Texture(Gdx.files.internal("turret/turret.png"))), new Sprite(new Texture(Gdx.files.internal("turret/turret.png")))}));
		ResourceManager.loadResource("casilla", new Animation(0.15f, new TextureRegion[]{
				new Sprite(new Texture(Gdx.files.internal("turret/casilla.png"))), new Sprite(new Texture(Gdx.files.internal("turret/casilla.png")))}));
		ResourceManager.loadResource("zombie", new Animation(0.15f, new TextureRegion[]{
				new Sprite(new Texture(Gdx.files.internal("enemy/zombie1.png"))), new Sprite(new Texture(Gdx.files.internal("enemy/zombie2.png")))
				, new Sprite(new Texture(Gdx.files.internal("enemy/zombie3.png"))), new Sprite(new Texture(Gdx.files.internal("enemy/zombie4.png")))}));
		ResourceManager.loadResource("ghost", new Animation(0.15f, new TextureRegion[]{
				new Sprite(new Texture(Gdx.files.internal("enemy/ghost1.png"))), new Sprite(new Texture(Gdx.files.internal("enemy/ghost2.png"))), 
				new Sprite(new Texture(Gdx.files.internal("enemy/ghost3.png"))), new Sprite(new Texture(Gdx.files.internal("enemy/ghost4.png")))
				, new Sprite(new Texture(Gdx.files.internal("enemy/ghost5.png"))), new Sprite(new Texture(Gdx.files.internal("enemy/ghost6.png")))
				, new Sprite(new Texture(Gdx.files.internal("enemy/ghost7.png")))}));
		ResourceManager.loadResource("potenciado", new Animation(0.15f, new TextureRegion[]{
				new Sprite(new Texture(Gdx.files.internal("enemy/potenciado1.png"))), new Sprite(new Texture(Gdx.files.internal("enemy/potenciado2.png")))
				, new Sprite(new Texture(Gdx.files.internal("enemy/potenciado3.png"))), new Sprite(new Texture(Gdx.files.internal("enemy/potenciado4.png")))
				, new Sprite(new Texture(Gdx.files.internal("enemy/potenciado5.png"))), new Sprite(new Texture(Gdx.files.internal("enemy/potenciado6.png")))
				, new Sprite(new Texture(Gdx.files.internal("enemy/potenciado7.png"))), new Sprite(new Texture(Gdx.files.internal("enemy/potenciado8.png")))}));
		ResourceManager.loadResource("resistente", new Animation(0.15f, new TextureRegion[]{
				new Sprite(new Texture(Gdx.files.internal("enemy/resistente1.png"))), new Sprite(new Texture(Gdx.files.internal("enemy/resistente2.png")))
				, new Sprite(new Texture(Gdx.files.internal("enemy/resistente3.png"))), new Sprite(new Texture(Gdx.files.internal("enemy/resistente4.png")))}));
		
	
		ResourceManager.loadResource("shoot", Gdx.audio.newSound(Gdx.files.internal("sounds/shoot.wav")));
		ResourceManager.loadResource("death", Gdx.audio.newSound(Gdx.files.internal("sounds/death.mp3")));
	}
	
	
	
	/**
	 * Carga un recurso de imagen en memoria
	 * @param name
	 * @param resource
	 */
	public static void loadResource(String name, Texture resource) {
		
		textures.put(name, resource);
	}
	
	/**
	 * Carga un recurso de animación en memoria
	 * @param name
	 * @param resource
	 */
	public static void loadResource(String name, Animation resource) {
		
		animations.put(name, resource);
	}
	
	/**
	 * Carga un recurso de sonido en memoria
	 * @param name
	 * @param sound
	 */
	public static void loadResource(String name, Sound sound) {
		
		sounds.put(name, sound);
	}
	
	/**
	 * Obtiene un recurso de imagen de memoria
	 * @param name
	 * @return
	 */
	public static Texture getTexture(String name) {
		
		return textures.get(name);
	}
	
	/**
	 * Obtiene un recurso de animación de memoria
	 * @param name
	 * @return
	 */
	public static Animation getAnimation(String name) {
		
		return animations.get(name);
	}
 
	/**
	 * Obtiene un recurso de sonido de memoria
	 * @param name
	 * @return
	 */
	public static Sound getSound(String name) {
		
		return sounds.get(name);
	}
}
