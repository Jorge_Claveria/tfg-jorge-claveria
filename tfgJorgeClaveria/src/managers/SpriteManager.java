package managers;

import static util.Constants.SCREEN_HEIGHT;
import static util.Constants.SCREEN_WIDTH;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;


import characters.Bala;
import characters.Casilla;
import characters.Enemigo;
import characters.EnemigoFantasma;
import characters.EnemigoNormal;
import characters.EnemigoPotenciado;
import characters.EnemigoResistente;
import characters.Tesla;
import characters.TorretaDefensa;
import characters.TorretaJugador;
import characters.Trampa;
import characters.Valla;
import screens.GameOverScreen;
import screens.MainMenuScreen;
import tfgJorgeClaveria.TfgJorgeClaveria;
import util.Constants;

/**
 * Genera los elementos en pantalla
 * @author Jorge Claveria
 */
public class SpriteManager {
	private float moveTimeNormal;
	private float moveTimeFantasma;
	private float moveTimePotenciado;
	private float moveTimeResistente;
	private Texture background;
	public TorretaJugador turret;
	public Casilla casilla;
	public EnemigoNormal denemigoNormal;
	public EnemigoFantasma denemigoFantasma;
	public EnemigoPotenciado denemigoPotenciado;
	public EnemigoResistente denemigoResistente;
	public LevelManager levelManager;
	
	public boolean finishLevel;



	boolean tocar;
	
	
	private TfgJorgeClaveria game;
	/**
	 * Metodo constructor del SpriteManager
	 * @param game
	 */
	public SpriteManager(TfgJorgeClaveria game) {
		background = ResourceManager.getTexture("background");
		
		turret = new TorretaJugador(50, 460, 230, game.configurationManager);
		turret.setLives(Constants.TURRET_LIVES);
		casilla = new Casilla(220,450,250,game.configurationManager);
		denemigoNormal = new EnemigoNormal(750,460,50);
		denemigoFantasma = new EnemigoFantasma(750,380,50);
		denemigoPotenciado = new EnemigoPotenciado(750,300,50);
		denemigoResistente = new EnemigoResistente(750,220,50);
		levelManager= new LevelManager(game.level);
		levelManager.readCurrentLevelFile();
		
		this.game = game;
		
	}
	
	public TorretaJugador getTurret() {
		return turret;
	}
	
	public Casilla getCasilla(){
		return casilla;
	}
	
	
	/**
	 * Actualiza la posición de los elementos de la pantalla y controla las vidas del jugador
	 * @param dt
	 */
	public void update(float dt) {
		levelManager.generateLevelFromFile(dt);
		moveTimeNormal += dt;
		moveTimeFantasma += dt;
		moveTimePotenciado += dt;
		moveTimeResistente += dt;
		updateTurretBullets(dt);
		tocar = false;
		for(Enemigo enemigo : levelManager.getEnemies()){
			if(enemigo.getClass().getSimpleName().equals("EnemigoNormal")){
				checkNormal(dt,(EnemigoNormal) enemigo);
			}
			if(enemigo.getClass().getSimpleName().equals("EnemigoFantasma")){
				checkFantasma(dt,(EnemigoFantasma) enemigo);
			}
			if(enemigo.getClass().getSimpleName().equals("EnemigoPotenciado")){
				checkPotenciado(dt,(EnemigoPotenciado) enemigo);
			}
			if(enemigo.getClass().getSimpleName().equals("EnemigoResistente")){
				checkResistente(dt,(EnemigoResistente) enemigo);
			}
			if(enemigo.getX()<=0){
				turret.hit();
				ResourceManager.getSound("death").play();
				enemigo.setX(860);
				enemigo.setLives(0);
			}
			
		}
		if(levelManager.isFinishLevel()||turret.getLives()<0){
			game.score=casilla.getPuntuacion();
			game.setScreen(new GameOverScreen(game));
			if(game.level<2){
				game.level++;
			}
		}
		
		
	


	}
	
	
	
	/**
	 * Actualiza el estado de los proyectiles del personaje
	 * @param dt
	 */
	private void updateTurretBullets(float dt) {
		
		List<Bala> bullets = turret.getBullets();
		List<Bala> defensaBala = new ArrayList<Bala>();
		for(TorretaDefensa torretaDefensa : casilla.getTorretaDefensa()){
			for(Bala bullet:torretaDefensa.getBullets()){
				defensaBala.add(bullet);
			}
		}
		
		Bala bullet = null;
		
		for (int i = bullets.size() - 1; i >= 0; i--) {
			
			bullet = bullets.get(i);
			bullet.update(dt);
			
			// Si el proyectil sale de la pantalla se elimina
			if ((bullet.getX() < 0) || (bullet.getX() > SCREEN_WIDTH) || (bullet.getY() < 0) || (bullet.getY() > SCREEN_HEIGHT))
				bullets.remove(i);
		}
		for (int i = defensaBala.size() - 1; i >= 0; i--) {
			
			bullet = defensaBala.get(i);
			bullet.update(dt);
			
			// Si el proyectil sale de la pantalla se elimina
			if ((bullet.getX() < 0) || (bullet.getX() > SCREEN_WIDTH) || (bullet.getY() < 0) || (bullet.getY() > SCREEN_HEIGHT))
				defensaBala.remove(i);
		}
		
	}

	
	/**
	 * Pinta en pantalla todos los elementos del juego
	 */
	public void draw(SpriteBatch batch) {
		
		batch.disableBlending();
		batch.draw(background, 0, 0);
		batch.enableBlending();
		
		turret.draw(batch);
		casilla.draw(batch);
		
		
		for(Enemigo enemigo : levelManager.getEnemies()){
			if(enemigo.getClass().getSimpleName().equals("EnemigoNormal")){
				denemigoNormal = (EnemigoNormal) enemigo;
				denemigoNormal.draw(batch);
			}
			if(enemigo.getClass().getSimpleName().equals("EnemigoFantasma")){
				denemigoFantasma = (EnemigoFantasma) enemigo;
				denemigoFantasma.draw(batch);
			}
			if(enemigo.getClass().getSimpleName().equals("EnemigoPotenciado")){
				denemigoPotenciado = (EnemigoPotenciado) enemigo;
				denemigoPotenciado.draw(batch);
			}
			if(enemigo.getClass().getSimpleName().equals("EnemigoResistente")){
				denemigoResistente = (EnemigoResistente) enemigo;
				denemigoResistente.draw(batch);
			}
			
		}
	
		
		for (Bala bullet : turret.getBullets()) {
			bullet.draw(batch);
		}
		for(Valla valla : casilla.getVallas()){
			valla.draw(batch);
		}
		for(Tesla tesla : casilla.getTesla()){
			tesla.draw(batch);
		}
		for(Trampa trampa : casilla.getTrampas()){
			trampa.draw(batch);
		}
		for(TorretaDefensa torretaDefensa : casilla.getTorretaDefensa()){
			torretaDefensa.draw(batch);
			for(Bala bullet:torretaDefensa.getBullets()){
				bullet.draw(batch);
			}
		}
		
		
	}
	/**
	 * Comprueba las colisiones de los enemigos normales
	 * @param dt
	 * @param enemigoNormal
	 */
	public void checkNormal(float dt,EnemigoNormal enemigoNormal){
		if(enemigoNormal.getLives()>0){
			for (int i = casilla.getVallas().size() - 1; i >= 0; i--) {
				if(enemigoNormal.getRect().overlaps(casilla.getVallas().get(i).getRect())){
					tocar=true;
					if(moveTimeNormal >=0.4f){
						moveTimeNormal=0;
					casilla.getVallas().get(i).hit();
					
					if(casilla.getVallas().get(i).getLives()<=0){
						casilla.getVallas().remove(i);
					}
					}
				}
			}
			for (int i = casilla.getTorretaDefensa().size() - 1; i >= 0; i--) {
				if(enemigoNormal.getY()==casilla.getTorretaDefensa().get(i).getY()+10){
					
					casilla.getTorretaDefensa().get(i).shoot(dt);
					
				}
				if(enemigoNormal.getRect().overlaps(casilla.getTorretaDefensa().get(i).getRect())){
					tocar=true;
					if(moveTimeNormal >=0.4f){
						moveTimeNormal=0;
					casilla.getTorretaDefensa().get(i).hit();
					System.out.println("Tocado");
					
					if(casilla.getTorretaDefensa().get(i).getLives()<=0){
						System.out.println("Muerto");
						casilla.getTorretaDefensa().remove(i);
					}
					}
				}
				
			}
			for (int i = casilla.getTesla().size() - 1; i >= 0; i--) {
				if(enemigoNormal.getX()-casilla.getTesla().get(i).getRectX()<70 && enemigoNormal.getY()-casilla.getTesla().get(i).getRectY()<70){	
				if(enemigoNormal.getRect().overlaps(casilla.getTesla().get(i).getRect())){
					tocar=true;
					if(moveTimeNormal >=0.4f){
						moveTimeNormal=0;
					casilla.getTesla().get(i).hit();
					enemigoNormal.hit();
					System.out.println("Tocado");
					
					if(casilla.getTesla().get(i).getLives()<=0){
						System.out.println("Muerto");
						casilla.getTesla().remove(i);
					}
					if(enemigoNormal.getLives()<=0){
						System.out.println("esta muerto");
						casilla.setPuntuacion(casilla.getPuntuacion()+500);
						enemigoNormal.setX(860);
					}
					}
				}else{
					enemigoNormal.hit();
					if(enemigoNormal.getLives()<=0){
						System.out.println("esta muerto");
						casilla.setPuntuacion(casilla.getPuntuacion()+500);
						enemigoNormal.setX(860);
					}
				}
				
			}
			}
			for (int i = casilla.getTrampas().size() - 1; i >= 0; i--) {
				if(enemigoNormal.getRect().overlaps(casilla.getTrampas().get(i).getRect())){
					tocar=true;
					if(moveTimeNormal >=0.4f){
						moveTimeNormal=0;
					casilla.getTrampas().get(i).hit();
					enemigoNormal.setLives(0);
					casilla.setPuntuacion(casilla.getPuntuacion()+500);
					enemigoNormal.setX(860);
					
					if(casilla.getTrampas().get(i).getLives()<=0){
						casilla.getTrampas().remove(i);
					}
					}
				}
			}
			enemigoNormal.update(dt,tocar);
			for (int i = turret.getBullets().size() - 1; i >= 0; i--) {
				if(turret.getBullets().get(i).getRect().overlaps(enemigoNormal.getRect())){
					enemigoNormal.hit();
					System.out.println(enemigoNormal.getLives());
					turret.getBullets().remove(i);
					if(enemigoNormal.getLives()<=0){
						System.out.println("esta muerto");
						casilla.setPuntuacion(casilla.getPuntuacion()+500);
						enemigoNormal.setX(860);
					}
				}
			}
			for(int i =casilla.getTorretaDefensa().size()-1;i>=0;i--){
				for(int j=casilla.getTorretaDefensa().get(i).getBullets().size()-1;j>=0;j--){
					if(casilla.getTorretaDefensa().get(i).getBullets().get(j).getRect().overlaps(enemigoNormal.getRect())){
					enemigoNormal.hit();
					System.out.println(enemigoNormal.getLives());
					casilla.getTorretaDefensa().get(i).getBullets().remove(j);
					if(enemigoNormal.getLives()<=0){
						System.out.println("esta muerto");
						casilla.setPuntuacion(casilla.getPuntuacion()+500);
						enemigoNormal.setX(860);
					}
				}
			}
			}
			
			}
		tocar = false;
	}
	/**
	 * Comprueba las colisiones de los enemigos fantasma
	 * @param dt
	 * @param enemigoFantasma
	 */
	public void checkFantasma(Float dt,EnemigoFantasma enemigoFantasma){
if(enemigoFantasma.getLives()>0){
			
			
			for (int i = casilla.getTesla().size() - 1; i >= 0; i--) {
				if(enemigoFantasma.getX()-casilla.getTesla().get(i).getRectX()<70 && enemigoFantasma.getY()-casilla.getTesla().get(i).getRectY()<70){	
				if(enemigoFantasma.getRect().overlaps(casilla.getTesla().get(i).getRect())){
					tocar=true;
					if(moveTimeFantasma >=0.4f){
						moveTimeFantasma=0;
					casilla.getTesla().get(i).hit();
					enemigoFantasma.hit();
					System.out.println("Tocado");
					
					if(casilla.getTesla().get(i).getLives()<=0){
						System.out.println("Muerto");
						casilla.getTesla().remove(i);
					}
					if(enemigoFantasma.getLives()<=0){
						System.out.println("esta muerto");
						casilla.setPuntuacion(casilla.getPuntuacion()+500);
						enemigoFantasma.setX(860);
					}
					}
				}else{
					enemigoFantasma.hit();
					if(enemigoFantasma.getLives()<=0){
						System.out.println("esta muerto");
						casilla.setPuntuacion(casilla.getPuntuacion()+500);
						enemigoFantasma.setX(860);
					}
				}
				
			}
			}
			
			enemigoFantasma.update(dt,tocar);
			for (int i = turret.getBullets().size() - 1; i >= 0; i--) {
				if(turret.getBullets().get(i).getRect().overlaps(enemigoFantasma.getRect())){
					enemigoFantasma.hit();
					System.out.println(enemigoFantasma.getLives());
					turret.getBullets().remove(i);
					if(enemigoFantasma.getLives()<=0){
						System.out.println("esta muerto");
						casilla.setPuntuacion(casilla.getPuntuacion()+500);
						enemigoFantasma.setX(860);
					}
				}
			}
			
			}
		tocar = false;
	}
	/**
	 * Metodo para comprobar las colisiones de los enemigos resistentes
	 * @param dt
	 * @param enemigoResistente
	 */
	public void checkResistente(Float dt,EnemigoResistente enemigoResistente){
		if(enemigoResistente.getLives()>0){
			for (int i = casilla.getVallas().size() - 1; i >= 0; i--) {
				if(enemigoResistente.getRect().overlaps(casilla.getVallas().get(i).getRect())){
					tocar=true;
					if(moveTimeResistente >=0.4f){
						moveTimeResistente=0;
					casilla.getVallas().get(i).hit();
					
					if(casilla.getVallas().get(i).getLives()<=0){
						casilla.getVallas().remove(i);
					}
					}
				}
			}
			for (int i = casilla.getTorretaDefensa().size() - 1; i >= 0; i--) {
				if(enemigoResistente.getY()==casilla.getTorretaDefensa().get(i).getY()+10){
					
					casilla.getTorretaDefensa().get(i).shoot(dt);
					
				}
				if(enemigoResistente.getRect().overlaps(casilla.getTorretaDefensa().get(i).getRect())){
					tocar=true;
					if(moveTimeResistente >=0.4f){
						moveTimeResistente=0;
					casilla.getTorretaDefensa().get(i).hit();
					System.out.println("Tocado");
					
					if(casilla.getTorretaDefensa().get(i).getLives()<=0){
						System.out.println("Muerto");
						casilla.getTorretaDefensa().remove(i);
					}
					}
				}
				
			}
			for (int i = casilla.getTesla().size() - 1; i >= 0; i--) {
				if(enemigoResistente.getX()-casilla.getTesla().get(i).getRectX()<70 && enemigoResistente.getY()-casilla.getTesla().get(i).getRectY()<70){	
				if(enemigoResistente.getRect().overlaps(casilla.getTesla().get(i).getRect())){
					tocar=true;
					if(moveTimeResistente >=0.4f){
						moveTimeResistente=0;
					casilla.getTesla().get(i).hit();
					enemigoResistente.hit();
					System.out.println("Tocado");
					
					if(casilla.getTesla().get(i).getLives()<=0){
						System.out.println("Muerto");
						casilla.getTesla().remove(i);
					}
					if(enemigoResistente.getLives()<=0){
						System.out.println("esta muerto");
						casilla.setPuntuacion(casilla.getPuntuacion()+500);
						enemigoResistente.setX(860);
					}
					}
				}else{
					enemigoResistente.hit();
					if(enemigoResistente.getLives()<=0){
						System.out.println("esta muerto");
						casilla.setPuntuacion(casilla.getPuntuacion()+500);
						enemigoResistente.setX(860);
					}
				}
				
			}
			}
			for (int i = casilla.getTrampas().size() - 1; i >= 0; i--) {
				if(enemigoResistente.getRect().overlaps(casilla.getTrampas().get(i).getRect())){
					tocar=true;
					if(moveTimeResistente >=0.4f){
						moveTimeResistente=0;
					casilla.getTrampas().get(i).hit();
					enemigoResistente.setLives(0);
					casilla.setPuntuacion(casilla.getPuntuacion()+500);
					enemigoResistente.setX(860);
					
					if(casilla.getTrampas().get(i).getLives()<=0){
						casilla.getTrampas().remove(i);
					}
					}
				}
			}
			enemigoResistente.update(dt,tocar);
			for (int i = turret.getBullets().size() - 1; i >= 0; i--) {
				if(turret.getBullets().get(i).getRect().overlaps(enemigoResistente.getRect())){
					enemigoResistente.hit();
					System.out.println(enemigoResistente.getLives());
					turret.getBullets().remove(i);
					if(enemigoResistente.getLives()<=0){
						System.out.println("esta muerto");
						casilla.setPuntuacion(casilla.getPuntuacion()+500);
						enemigoResistente.setX(860);
					}
				}
			}
			for(int i =casilla.getTorretaDefensa().size()-1;i>=0;i--){
				for(int j=casilla.getTorretaDefensa().get(i).getBullets().size()-1;j>=0;j--){
					if(casilla.getTorretaDefensa().get(i).getBullets().get(j).getRect().overlaps(enemigoResistente.getRect())){
					enemigoResistente.hit();
					System.out.println(enemigoResistente.getLives());
					casilla.getTorretaDefensa().get(i).getBullets().remove(j);
					if(enemigoResistente.getLives()<=0){
						System.out.println("esta muerto");
						casilla.setPuntuacion(casilla.getPuntuacion()+500);
						enemigoResistente.setX(860);
					}
				}
			}
			}
			
			}
		tocar = false;
	}
	/**
	 * Comprueba las colisiones de los enemigos potenciados
	 * @param dt
	 * @param enemigoPotenciado
	 */
	public void checkPotenciado(Float dt,EnemigoPotenciado enemigoPotenciado){
		if(enemigoPotenciado.getLives()>0){
			for (int i = casilla.getVallas().size() - 1; i >= 0; i--) {
				if(enemigoPotenciado.getRect().overlaps(casilla.getVallas().get(i).getRect())){
					tocar=true;
					if(moveTimePotenciado >=0.4f){
						moveTimePotenciado=0;
					
					casilla.getVallas().get(i).setLives(casilla.getVallas().get(i).getLives()-10);
					
					
					if(casilla.getVallas().get(i).getLives()<=0){
						casilla.getVallas().remove(i);
					}
					}
				}
			}
			for (int i = casilla.getTorretaDefensa().size() - 1; i >= 0; i--) {
				if(enemigoPotenciado.getY()==casilla.getTorretaDefensa().get(i).getY()+10){
					
					casilla.getTorretaDefensa().get(i).shoot(dt);
					
				}
				if(enemigoPotenciado.getRect().overlaps(casilla.getTorretaDefensa().get(i).getRect())){
					tocar=true;
					if(moveTimePotenciado >=0.4f){
						moveTimePotenciado=0;
						casilla.getTorretaDefensa().get(i).setLives(casilla.getTorretaDefensa().get(i).getLives()-10);
					System.out.println("Tocado");
					
					if(casilla.getTorretaDefensa().get(i).getLives()<=0){
						System.out.println("Muerto");
						casilla.getTorretaDefensa().remove(i);
					}
					}
				}
				
			}
			for (int i = casilla.getTesla().size() - 1; i >= 0; i--) {
				if(enemigoPotenciado.getX()-casilla.getTesla().get(i).getRectX()<70 && enemigoPotenciado.getY()-casilla.getTesla().get(i).getRectY()<70){	
				if(enemigoPotenciado.getRect().overlaps(casilla.getTesla().get(i).getRect())){
					tocar=true;
					if(moveTimePotenciado >=0.4f){
						moveTimePotenciado=0;
						casilla.getTesla().get(i).setLives(casilla.getTesla().get(i).getLives()-10);
					enemigoPotenciado.hit();
					System.out.println("Tocado");
					
					if(casilla.getTesla().get(i).getLives()<=0){
						System.out.println("Muerto");
						casilla.getTesla().remove(i);
					}
					if(enemigoPotenciado.getLives()<=0){
						System.out.println("esta muerto");
						casilla.setPuntuacion(casilla.getPuntuacion()+500);
						enemigoPotenciado.setX(860);
					}
					}
				}else{
					enemigoPotenciado.hit();
					if(enemigoPotenciado.getLives()<=0){
						System.out.println("esta muerto");
						casilla.setPuntuacion(casilla.getPuntuacion()+500);
						enemigoPotenciado.setX(860);
					}
				}
				
			}
			}
			for (int i = casilla.getTrampas().size() - 1; i >= 0; i--) {
				if(enemigoPotenciado.getRect().overlaps(casilla.getTrampas().get(i).getRect())){
					tocar=true;
					if(moveTimePotenciado >=0.4f){
						moveTimePotenciado=0;
					casilla.getTrampas().get(i).hit();
					enemigoPotenciado.setLives(0);
					casilla.setPuntuacion(casilla.getPuntuacion()+500);
					enemigoPotenciado.setX(860);
					
					if(casilla.getTrampas().get(i).getLives()<=0){
						casilla.getTrampas().remove(i);
					}
					}
				}
			}
			enemigoPotenciado.update(dt,tocar);
			for (int i = turret.getBullets().size() - 1; i >= 0; i--) {
				if(turret.getBullets().get(i).getRect().overlaps(enemigoPotenciado.getRect())){
					enemigoPotenciado.hit();
					System.out.println(enemigoPotenciado.getLives());
					turret.getBullets().remove(i);
					if(enemigoPotenciado.getLives()<=0){
						System.out.println("esta muerto");
						casilla.setPuntuacion(casilla.getPuntuacion()+500);
						enemigoPotenciado.setX(860);
					}
				}
			}
			for(int i =casilla.getTorretaDefensa().size()-1;i>=0;i--){
				for(int j=casilla.getTorretaDefensa().get(i).getBullets().size()-1;j>=0;j--){
					if(casilla.getTorretaDefensa().get(i).getBullets().get(j).getRect().overlaps(enemigoPotenciado.getRect())){
					enemigoPotenciado.hit();
					System.out.println(enemigoPotenciado.getLives());
					casilla.getTorretaDefensa().get(i).getBullets().remove(j);
					if(enemigoPotenciado.getLives()<=0){
						System.out.println("esta muerto");
						casilla.setPuntuacion(casilla.getPuntuacion()+500);
						enemigoPotenciado.setX(860);
					}
				}
			}
			}
			
			}
		tocar = false;
	}
	
	
	
	
	}

	
