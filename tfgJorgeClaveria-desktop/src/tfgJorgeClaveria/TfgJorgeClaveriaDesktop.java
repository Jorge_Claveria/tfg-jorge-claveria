  package tfgJorgeClaveria;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import tfgJorgeClaveria.TfgJorgeClaveria;
import util.Constants;
/**
 * Clase que ejecuta el videojuego en PC
 * @author usuario
 *
 */
public class TfgJorgeClaveriaDesktop {

	public static void main(String[] args) {
		LwjglApplicationConfiguration configuracion = new LwjglApplicationConfiguration();
		configuracion.title = "TANKS VS MONSTERS";
		configuracion.width = Constants.SCREEN_WIDTH;
		configuracion.height = Constants.SCREEN_HEIGHT;
		
		configuracion.fullscreen = false;
				
		new LwjglApplication(new TfgJorgeClaveria(), configuracion);
	}
}

